import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {BankAccountService} from "../service/bank-account.service";
import {AuthService} from "../service/auth.service";
import {Type} from "../model/type";
import {MatSnackBar} from "@angular/material/snack-bar";
import {TransactionService} from "../service/transaction.service";

@Component({
  selector: 'app-new-transaction',
  templateUrl: './new-transaction.component.html',
  styleUrls: ['./new-transaction.component.css']
})
export class NewTransactionComponent {

  transactionTypes = Type
  enumKeys = [];
  cardLimit: number
  clientAccounts: [];
  userName: string;

  constructor(private router: Router, private accountService: BankAccountService,
              private authService: AuthService,
              private snackBar: MatSnackBar,
              private transactionService: TransactionService) {
    this.enumKeys = Object.keys(this.transactionTypes).filter(f => !isNaN(Number(f)));
  }

  ngOnInit(): void {
    this.userName = this.getCurrentUser()
    this.getClientsCurrentAccounts();
  }

  transactionForm: FormGroup = new FormGroup({
    transactionType: new FormControl('', Validators.required),
    bank_account_id: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.required),
  });


  public getClientsCurrentAccounts() {
    return this.accountService.getClientsAccounts(this.userName).subscribe({
      next: (res: any) => {
        this.clientAccounts = res
      }
    })
  }

  public getCurrentUser() {
    return this.authService.getCurrentUser();
  }

  back() {
    this.router.navigate(['/home'])
  }


  saveForm() {
    this.transactionService.createTransaction(this.transactionForm.value).subscribe({
      next: (res: any) => {
        this.snackBar.open('Your transaction was successful.',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['success']
          });

      },
      error: err => {
        this.snackBar.open('Your transaction was unsuccessful! Please try again! ',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['error']
          });
      },
      complete: () => {
        this.router.navigate(['/home'])
      }
    })

  }
}
