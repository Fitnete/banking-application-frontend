import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from "../model/user";
import {MatPaginator} from "@angular/material/paginator";
import {UserService} from "../service/user.service";
import {Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {
  }

  displayedColumns: string[] = [
    'userName',
    'firstName',
    'lastName',
    'role',
    'actions',
  ];

  @ViewChild('paginator', {static: false})
  paginator: MatPaginator;

  tellers: MatTableDataSource<User>;
  isNewTeller = true


  ngOnInit(): void {
    this.getTellers();
  }


  back() {
    this.router.navigate(['/home'])
  }

  getTellers() {
    this.userService.getTellers().subscribe({
      next: (res: any) => {
        this.tellers = new MatTableDataSource<User>(res);
        this.tellers.paginator = this.paginator;
      },
      error: (err: any) => {
        console.log(err)
      },
      complete: () => {
      }
    })
  }

  updateTeller(userName: string) {
    this.router.navigate(['/new-teller', {userName: userName}]);

  }

  deleteTeller(userName: string) {
    this.userService.deleteTeller(userName).subscribe({
      next: (res: any) => {
      }, error: err => {
        console.log(err)
      },
      complete: () => {
        this.getTellers();
      }
    });
  }
}
