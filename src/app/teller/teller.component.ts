import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from "../model/user";
import {MatPaginator} from "@angular/material/paginator";
import {Router} from "@angular/router";
import {UserService} from "../service/user.service";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-teller',
  templateUrl: './teller.component.html',
  styleUrls: ['./teller.component.css']
})
export class TellerComponent implements OnInit {
  constructor(private userService: UserService, private router: Router) {
  }

  clients: MatTableDataSource<User>;
  displayedColumns: string[] = [
    'userName',
    'firstName',
    'lastName',
    'role',
    'actions',
  ];
  @ViewChild('paginator', {static: false})
  paginator: MatPaginator;
  isNewClient: true;

  ngOnInit(): void {
    this.getClients();
  }

  back() {
    this.router.navigate(['/home']);
  }

  getClients() {
    this.userService.getClients().subscribe({
      next: (res: any) => {
        this.clients = new MatTableDataSource<User>(res);
        this.clients.paginator = this.paginator;

      },
      error: (err: any) => {
        console.log(err)
      },
      complete: () => {
      }
    });
  }

  updateClient(userName: string) {
    this.router.navigate(['/new-client', {userName: userName}]);

  }

  deleteClient(userName: string) {
    this.userService.deleteClient(userName).subscribe({
      next: (res: any) => {
      }, error: err => {
        console.log(err)
      },
      complete: () => {
        this.getClients();
      }
    });
  }
}
