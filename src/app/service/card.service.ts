import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Card} from "../model/card";

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private httpClient: HttpClient) {
  }

  API = 'http://localhost:8080';

  public getCardTypes() {
    return this.httpClient.get(this.API + '/cardTypes')
  }

  public createDebitCard(cardModel: Card) {
    return this.httpClient.post<Card>(this.API + '/new-debit-card', cardModel)
  }

  public createCreditCard(cardModel: Card) {
    return this.httpClient.post<Card>(this.API + '/new-credit-card', cardModel)
  }

  getAllCards() {
    return this.httpClient.get(this.API + '/cards')
  }

  public approveCard(cardModel: Card) {
    return this.httpClient.post<Card>(this.API + '/approve-card/', cardModel)
  }

  getClientCard(id: number) {
    return this.httpClient.get(this.API + '/card/' + id)
  }

  getClientCards(user_id: string) {
    return this.httpClient.get(this.API + '/cards/' + user_id)
  }
}
