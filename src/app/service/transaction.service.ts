import {Injectable} from '@angular/core';

import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Transaction} from "../model/transaction";

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  API = 'http://localhost:8080';

  constructor(private httpClient: HttpClient,) {
  }

  public createTransaction(transaction: Transaction): Observable<Transaction> {
    return this.httpClient.post<Transaction>(this.API + '/new-transaction', transaction)
  }

  getClientsTransactions(user_id: string) {
    return this.httpClient.get(this.API + '/transactions/' + user_id)
  }

  getAllTransactions() {
    return this.httpClient.get(this.API + '/transactions')
  }
}
