import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthService} from './auth.service';
import {User} from "../model/user";
import {Role} from "../model/role";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {


  API = 'http://localhost:8080';
  requestHeader = new HttpHeaders({'No-Auth': 'True'});

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {
  }

  public login({userName, userPassword}) {
    return this.httpClient.post(this.API + '/authenticate', {userName, userPassword}, {
      headers: this.requestHeader,
    });
  }

  public getTellers() {
    return this.httpClient.get<User[]>(this.API + '/tellers')
  }

  public getClients() {
    return this.httpClient.get<User[]>(this.API + '/clients')
  }

  public roleMatch(allowedRoles: any) {
    let isMatch = false;
    const roles: any = this.authService.getRoles();

    if (roles != null && roles) {
      for (let i = 0; i < roles.length; i++) {
        for (let j = 0; j < allowedRoles.length; j++) {
          if (roles[i]['roleName'] === allowedRoles[j]) {
            isMatch = true;
            return isMatch;
          } else {
            return isMatch;
          }
        }
      }
    }
  }

  public getRoles() {
    return this.httpClient.get<Role[]>(this.API + '/roles')
  }

  public createClient(client: User): Observable<User> {
    return this.httpClient.post<User>(this.API + '/createClient', client)

  }

  public createTeller(teller: User): Observable<User> {
    return this.httpClient.post<User>(this.API + '/createTeller', teller)

  }

  public getTeller(userName) {
    return this.httpClient.get<User>(this.API + '/getTeller/' + userName);
  }

  public getClient(userName) {
    return this.httpClient.get<User>(this.API + '/getClient/' + userName);
  }

  deleteTeller(userName: string) {
    return this.httpClient.delete<User>(this.API + '/deleteTeller/' + userName)
  }

  deleteClient(userName: string) {
    return this.httpClient.delete<User>(this.API + '/deleteClient/' + userName);

  }

}
