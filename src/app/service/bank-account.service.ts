import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BankAccount} from "../model/bank-account";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BankAccountService {
  API = 'http://localhost:8080';

  constructor(private httpClient: HttpClient,) {
  }

  public createBankAccount(bankAccount: BankAccount): Observable<BankAccount> {
    return this.httpClient.post<BankAccount>(this.API + '/new-account', bankAccount)
  }

  public getCurrencies() {
    return this.httpClient.get(this.API + '/currencies')

  }

  getClientsAccounts(user_id: string) {
    return this.httpClient.get(this.API + '/accounts/' + user_id)
  }

  getAllAccounts() {
    return this.httpClient.get(this.API + '/accounts')
  }

  getClientAccount(bankAccountId: number) {
    return this.httpClient.get(this.API + '/account/' + bankAccountId)
  }

  approveAccount(bankAccount: BankAccount, bankAccountId: number) {
    return this.httpClient.put<BankAccount>(this.API + '/updateAccount/' + bankAccountId, bankAccount)
  }


}
