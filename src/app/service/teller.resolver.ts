import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {UserService} from "./user.service";
import {User} from "../model/user";

@Injectable({
  providedIn: 'root'
})
export class TellerResolver implements Resolve<User> {
  constructor(
    private userService: UserService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<User> {
    const userName = route.paramMap.get('userName');
    if (userName) {
      //fetch data from backend for this username
      return this.userService.getTeller(userName)

    } else {
      return of(this.createEmptyUser());
    }

  }

  private createEmptyUser() {
    return undefined;
  }
}
