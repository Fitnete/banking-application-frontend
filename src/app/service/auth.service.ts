import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() {
  }

  public setRoles(roles: []) {
    localStorage.setItem('roles', JSON.stringify(roles));
  }

  public getRoles() {
    return JSON.parse(localStorage.getItem('roles') as string);
  }

  public setToken(jwtToken: string) {
    localStorage.setItem('jwtToken', jwtToken);
  }

  public getToken() {
    return localStorage.getItem('jwtToken');
  }


  public getCurrentUser() {
    return localStorage.getItem('userName');
  }

  public setCurrentUser(userName: string) {
    return localStorage.setItem('userName', userName);
  }

  public isAdmin(): boolean {
    const roles: any[] = this.getRoles();
    return roles[0].roleName === 'admin'
  }

  public isTeller(): boolean {
    const roles: any[] = this.getRoles();
    return roles[0].roleName === 'teller'
  }

  public isClient(): boolean {
    const roles: any[] = this.getRoles();
    return roles[0].roleName === 'client'
  }

  public clearStorage() {
    localStorage.clear();
  }

}
