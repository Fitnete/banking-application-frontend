import { TestBed } from '@angular/core/testing';

import { TellerResolver } from './teller.resolver';

describe('UserResolver', () => {
  let resolver: TellerResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(TellerResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
