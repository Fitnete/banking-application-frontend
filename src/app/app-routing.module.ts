import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {AdminComponent} from "./admin/admin.component";
import {AuthGuard} from "./auth/auth.guard";
import {TellerComponent} from "./teller/teller.component";
import {NewAccountComponent} from "./new-account/new-account.component";
import {NewCardComponent} from "./new-card/new-card.component";
import {NewTransactionComponent} from "./new-transaction/new-transaction.component";
import {NewClientComponent} from "./new-client/new-client.component";
import {NewTellerComponent} from "./new-teller/new-teller.component";
import {TellerResolver} from "./service/teller.resolver";
import {ClientResolver} from "./service/client.resolver";

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'home', component: HomeComponent},
  {
    path: 'admin', component: AdminComponent,
    canActivate: [AuthGuard],
    data: {roles: ['admin']},
  },
  {
    path: 'teller', component: TellerComponent,
    canActivate: [AuthGuard],
    data: {roles: ['teller']},
  },

  {
    path: 'new-account', component: NewAccountComponent,
    canActivate: [AuthGuard],
    data: {roles: ['client']},
  },
  {
    path: 'new-card', component: NewCardComponent,
    canActivate: [AuthGuard],
    data: {roles: ['client']},
  },
  {
    path: 'new-transaction', component: NewTransactionComponent,
    canActivate: [AuthGuard],
    data: {roles: ['client']},
  },
  {
    path: 'new-client', component: NewClientComponent,
    canActivate: [AuthGuard],
    data: {roles: ['teller']},
    resolve: {
      client: ClientResolver,
    },
  },
  {
    path: 'new-teller', component: NewTellerComponent,
    canActivate: [AuthGuard],
    data: {roles: ['admin']},
    resolve: {
      teller: TellerResolver,
    },
  }
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule {
}
