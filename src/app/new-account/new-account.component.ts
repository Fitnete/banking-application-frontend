import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BankAccountService} from "../service/bank-account.service";
import {Router} from "@angular/router";
import {AuthService} from "../service/auth.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css']
})
export class NewAccountComponent implements OnInit {

  constructor(private accountService: BankAccountService, private router: Router,
              private authService: AuthService, private snackBar: MatSnackBar) {
  }

  accountType = "CURRENT"
  currencies: []
  user_id: String

  ngOnInit(): void {
    this.user_id = this.getCurrentUser()
    this.accountService.getCurrencies().subscribe({
      next: (res: any) => {
        this.currencies = res
      }
    });
  }

  bankAccountForm: FormGroup = new FormGroup({
    user_id: new FormControl('', Validators.required),
    currency: new FormControl('', Validators.required),
    accountType: new FormControl('', Validators.required),
  });


  public saveForm() {
    this.accountService.createBankAccount(this.bankAccountForm.value).subscribe({
      next: (res: any) => {
        this.snackBar.open('Request was successful but is not yet approved by the teller.',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['success']
          });

      },
      error: err => {
        this.snackBar.open('Request was unsuccessful! Please try again! ',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['error']
          });
      },
      complete: () => {
        this.router.navigate(['/home'])
      }
    });
  }

  public back() {
    this.router.navigate(['/home'])
  }

  public getCurrentUser() {
    return this.authService.getCurrentUser();

  }
}
