import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../service/user.service";
import {AuthService} from "../service/auth.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup!: FormGroup;

  constructor(private userService: UserService,
              private authService: AuthService,
              private router: Router,
              private snackBar: MatSnackBar) {
  }


  ngOnInit(): void {
    this.initForm()
  }


  initForm() {
    this.formGroup = new FormGroup({
      userName: new FormControl('', [Validators.required]),
      userPassword: new FormControl('', [Validators.required]),

    })
  }

  login() {
    if (this.formGroup.valid) {
      this.userService.login(this.formGroup.value).subscribe({
        next: (res: any) => {
          this.authService.setRoles(res.user.roles);
          this.authService.setToken(res.jwtToken);
          this.authService.setCurrentUser(res.user.userName)
          this.snackBar.open('Login was successful', 'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['success']
          });
        },
        error: (err: any) => {
          console.log(err)
        },
        complete: () => {
          this.router.navigate(['/home'])
        }
      })
    } else {
      this.snackBar.open('Something went wrong', 'Close', {
        duration: 5000,
        verticalPosition: 'bottom',
        panelClass: ['error']
      });
    }
  }
}

