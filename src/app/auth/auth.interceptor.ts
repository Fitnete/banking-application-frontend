import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {catchError, Observable, throwError,} from 'rxjs';
import {AuthService} from "../service/auth.service";
import {Router} from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.headers.get('No-Auth') === 'True') {
      return next.handle(request.clone());
    }
    const token = this.authService.getToken();
    if (token) {
      request = this.addToken(request, token);
    }
    return next.handle(request).pipe(
      catchError(
        err => {
          console.log(err.status);
          if (err.status === 401) {
            this.router.navigate(['']);
          } else if (err.status === 403) {
            this.router.navigate(['/forbidden']);
          }
          return throwError(err);
        }
      )
    );
  }

  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone(
      {
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      }
    );
  }
}
