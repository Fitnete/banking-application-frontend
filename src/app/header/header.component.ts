import {Component} from '@angular/core';
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  constructor(private authService: AuthService, private router: Router) {
  }

  logOut() {
    this.authService.clearStorage();
    this.router.navigate(["/"])
  }

  public isAdmin() {
    return this.authService.isAdmin();
  }

  public isTeller() {
    return this.authService.isTeller();
  }

  public isClient() {
    return this.authService.isClient();
  }
}
