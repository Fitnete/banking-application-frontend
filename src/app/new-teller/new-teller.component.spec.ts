import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTellerComponent } from './new-teller.component';

describe('NewTellerComponent', () => {
  let component: NewTellerComponent;
  let fixture: ComponentFixture<NewTellerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewTellerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewTellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
