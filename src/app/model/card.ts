export interface Card {
  id: number;
  cardType: string;
  bank_account_id: number
  monthlySalary: number;
  cardLimit: number;
  approved: boolean
}
