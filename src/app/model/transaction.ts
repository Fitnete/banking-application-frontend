export interface Transaction {
  transactionId: number;
  bank_account_id: number;
  amount: number;
  currency: string;
  transactionType: string;
}

