export interface User {
  userName: string;
  firstName: string;
  lastName: string;
  userPassword: string;
  email: string;
  roles: [];
}
