export interface BankAccount {
  bankAccountId: number;
  user_id: string;
  currency: string;
  accountType: string;
  approved: boolean
  cardType: string;
}
