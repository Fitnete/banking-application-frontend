import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../service/auth.service";
import {MatPaginator} from "@angular/material/paginator";
import {BankAccountService} from "../service/bank-account.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CardService} from "../service/card.service";
import {TransactionService} from "../service/transaction.service";
import {BankAccount} from "../model/bank-account";
import {MatTableDataSource} from "@angular/material/table";
import {Card} from "../model/card";
import {Transaction} from "../model/transaction";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  clientBankAccounts: MatTableDataSource<BankAccount>;
  allBankAccounts: MatTableDataSource<BankAccount>;
  clientCards: MatTableDataSource<Card>;
  allCards: MatTableDataSource<Card>;
  clientTransactions: MatTableDataSource<Transaction>;
  allTransactions: MatTableDataSource<Transaction>;
  userName: string;


  tellerCardsDisplayedColumns: string[] = [
    'cardType',
    'cardLimit',
    'bank_account_id',
    'approved',
    'actions'
  ]
  clientCardsDisplayedColumns: string[] = [
    'cardType',
    'cardLimit',
    'bank_account_id',
    'approved',
  ]
  tellerDisplayedColumns: string[] = [
    'iban',
    'currency',
    'balance',
    'accountType',
    'interest',
    'user_id',
    'approved',
    'actions'

  ];
  clientDisplayedColumns: string[] = [
    'bankAccountId',
    'iban',
    'currency',
    'balance',
    'accountType',
    'interest',
    'approved',
  ];
  clientTransactionsDisplayedColumns: string[] = [
    'bank_account_id',
    'amount',
    'currency',
    'transactionType'
  ]

  @ViewChild('paginator1', {static: false})
  paginator1: MatPaginator;

  @ViewChild('paginator2', {static: false})
  paginator2: MatPaginator;

  @ViewChild('paginator3', {static: false})
  paginator3: MatPaginator;

  @ViewChild('paginator4', {static: false})
  paginator4: MatPaginator;

  @ViewChild('paginator5', {static: false})
  paginator5: MatPaginator;

  @ViewChild('paginator6', {static: false})
  paginator6: MatPaginator;

  constructor(private authService: AuthService,
              private accountService: BankAccountService,
              private snackBar: MatSnackBar,
              private cardService: CardService,
              private transactionService: TransactionService) {
  }

  ngOnInit(): void {
    this.userName = this.getCurrentUser()
    this.getClientsAccounts();
    this.getAllAccounts();
    this.getAllCards();
    this.getClientCards();
    this.getClientsTransactions();
    this.getAllTransactions();
  }

  public isAdmin() {
    return this.authService.isAdmin();
  }

  public isTeller() {
    return this.authService.isTeller();
  }

  public isClient() {
    return this.authService.isClient();
  }

  public getCurrentUser() {
    return this.authService.getCurrentUser();
  }

  public getClientsAccounts() {
    return this.accountService.getClientsAccounts(this.userName).subscribe({
      next: (res: any) => {
        this.clientBankAccounts = new MatTableDataSource<BankAccount>(res);
        this.clientBankAccounts.paginator = this.paginator4;

      },
      error: err => {
        console.log(err)
      },
      complete: () => {
      }
    });
  }

  public getClientCards() {
    return this.cardService.getClientCards(this.userName).subscribe({
      next: (res: any) => {
        this.clientCards = new MatTableDataSource<Card>(res);
        this.clientCards.paginator = this.paginator5;
      },
      error: err => {
        console.log(err)
      },
      complete: () => {
      }
    });
  }

  public getClientsTransactions() {
    return this.transactionService.getClientsTransactions(this.userName).subscribe({
      next: (res: any) => {
        this.clientTransactions = new MatTableDataSource<Transaction>(res);
        this.clientTransactions.paginator = this.paginator6;
      },
      error: err => {
        console.log(err)
      },
      complete: () => {
      }
    });
  }

  public getAllAccounts() {
    return this.accountService.getAllAccounts().subscribe({
      next: (res: any) => {
        this.allBankAccounts = new MatTableDataSource<BankAccount>(res);
        this.allBankAccounts.paginator = this.paginator1;
      },
      error: err => {
        console.log(err)
      },
      complete: () => {
      }
    });
  }

  public getAllCards() {
    return this.cardService.getAllCards().subscribe({
      next: (res: any) => {
        this.allCards = new MatTableDataSource<Card>(res);
        ;
        this.allCards.paginator = this.paginator2;
      },
      error: err => {
        console.log(err)
      },
      complete: () => {
      }
    });
  }

  public getAllTransactions() {
    return this.transactionService.getAllTransactions().subscribe({
      next: (res: any) => {
        this.allTransactions = new MatTableDataSource<Transaction>(res);
        this.allTransactions.paginator = this.paginator3;
      },
      error: err => {
        console.log(err)
      },
      complete: () => {
      }
    });
  }

  public approveAccount(bankAccountId: number) {
    this.accountService.getClientAccount(bankAccountId).subscribe({
      next: (response: any) => {
        response.approved = true
        this.accountService.approveAccount(response, response.bankAccountId).subscribe({
          next: (res: any) => {
            this.snackBar.open('Approved Successfully', 'Close', {
              duration: 5000,
              verticalPosition: 'top',
              panelClass: ['success']
            });
          },
          error: (err: any) => {
            console.log(err)
            this.snackBar.open('Something went wrong', 'Close', {
              duration: 5000,
              verticalPosition: 'bottom',
              panelClass: ['error']
            });
          },
          complete: () => {
            this.getAllAccounts();
          }
        });
      }
    });
  }

  public approveCard(id: number) {
    this.cardService.getClientCard(id).subscribe({
      next: (response: any) => {
        response.approved = true
        this.cardService.approveCard(response).subscribe({
          next: (res: any) => {
            this.snackBar.open('Approved Successfully', 'Close', {
              duration: 5000,
              verticalPosition: 'top',
              panelClass: ['success']
            });
          },
          error: (err: any) => {
            console.log(err)
            this.snackBar.open('Something went wrong', 'Close', {
              duration: 5000,
              verticalPosition: 'bottom',
              panelClass: ['error']
            });
          },
          complete: () => {
            this.getAllCards();
            this.getAllAccounts();
          }
        });
      }
    });
  }
}
