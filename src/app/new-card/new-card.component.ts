import {Component, OnInit, ViewChild} from '@angular/core';
import {CardService} from "../service/card.service";
import {MatAccordion} from "@angular/material/expansion";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BankAccountService} from "../service/bank-account.service";
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";


@Component({
  selector: 'app-new-card',
  templateUrl: './new-card.component.html',
  styleUrls: ['./new-card.component.css']
})
export class NewCardComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private cardService: CardService,
              private accountService: BankAccountService,
              private authService: AuthService,
              private router: Router,
              private snackBar: MatSnackBar) {
  }

  cardTypes: []
  cardTypeDebit = "DEBIT"
  cardTypeCredit = "CREDIT"
  clientCurrentAccounts: [];
  userName: string;
  salary: number = 0;
  limit: number = 0;

  ngOnInit(): void {
    this.userName = this.getCurrentUser()
    this.getCardTypes();
    this.getClientsCurrentAccounts();
  }

  debitCardForm: FormGroup = new FormGroup({
    cardType: new FormControl('', Validators.required),
    bank_account_id: new FormControl('', Validators.required),
  });
  creditCardForm: FormGroup = new FormGroup({
    cardType: new FormControl('', Validators.required),
    monthlySalary: new FormControl(0, [Validators.required, Validators.min(500)]),
    cardLimit: new FormControl(0, [Validators.required, Validators.min(500)]),
  });


  public getCardTypes() {
    return this.cardService.getCardTypes().subscribe({
      next: (res: any) => {
      }
    })
  }

  public getClientsCurrentAccounts() {
    return this.accountService.getClientsAccounts(this.userName).subscribe({
      next: (res: any) => {
        this.clientCurrentAccounts = res.filter(account => account.accountType === "CURRENT")
      }
    })
  }

  public getCurrentUser() {
    return this.authService.getCurrentUser();
  }

  requestDebitCard() {
    this.cardService.createDebitCard(this.debitCardForm.value).subscribe({
      next: (res: any) => {
        this.snackBar.open('Request was successful but is not yet approved by the teller.',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['success']
          });
      },
      error: err => {
        this.snackBar.open('Request was unsuccessful! Please try again! ',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['error']
          });
      },
      complete: () => {
        this.router.navigate(['/home'])
      }
    });
  }

  requestCreditCard() {
    this.cardService.createCreditCard(this.creditCardForm.value).subscribe({
      next: (res: any) => {
        this.snackBar.open('Request was successful but is not yet approved by the teller.',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['success']
          });
      },
      error: err => {
        this.snackBar.open('Request was unsuccessful! Please try again! ',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['error']
          });
      },
      complete: () => {
        this.router.navigate(['/home'])
      }
    });
  }

  back() {
    this.router.navigate(['/home'])
  }
}
