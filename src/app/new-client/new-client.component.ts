import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../service/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {User} from "../model/user";

@Component({
  selector: 'app-new-client',
  templateUrl: './new-client.component.html',
  styleUrls: ['./new-client.component.css']
})

export class NewClientComponent implements OnInit {
  constructor(private userService: UserService,
              private router: Router,
              private snackBar: MatSnackBar,
              private activatedRoute: ActivatedRoute) {
  }

  user: User;
  userRoles: [];

  clientForm: FormGroup = new FormGroup({
    userName: new FormControl('', Validators.required),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    userPassword: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    roles: new FormControl('', Validators.required),
  });

  ngOnInit(): void {
    this.getRoles();
    this.user = this.activatedRoute.snapshot.data['client'];
    this.clientForm.setValue({
      userName: this.user.userName,
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      userPassword: this.user.userPassword,
      email: this.user.email,
      roles: this.user.roles
    });
  }

  saveForm() {
    this.userService.createClient(this.clientForm.value).subscribe({
      next: (res: any) => {
        this.snackBar.open('New client created successfully!',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['success']
          });

      },
      error: err => {
        this.snackBar.open('Creation was unsuccessful! Please try again! ',
          'Close', {
            duration: 5000,
            verticalPosition: 'top',
            panelClass: ['error']
          });
      },
      complete: () => {
        this.router.navigate(['/teller'])
      }
    });
  }

  back() {
    this.router.navigate(['/teller'])
  }

  public getRoles() {
    return this.userService.getRoles().subscribe({
      next: (res: any) => {
        this.userRoles = res;
      },
      error: err => {
        console.log(err)
      },
      complete: () => {
      }
    });
  }

}
